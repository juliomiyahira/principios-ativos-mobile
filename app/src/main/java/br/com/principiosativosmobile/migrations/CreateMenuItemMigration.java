package br.com.principiosativosmobile.migrations;

import android.database.sqlite.SQLiteDatabase;

import se.emilsjolander.sprinkles.Migration;
import se.emilsjolander.sprinkles.annotations.AutoIncrement;
import se.emilsjolander.sprinkles.annotations.Column;
import se.emilsjolander.sprinkles.annotations.Key;

/**
 * Created by julio on 25/09/15.
 */
public class CreateMenuItemMigration extends Migration {

    @Override
    protected void doMigration(SQLiteDatabase db) {
        db.execSQL(" CREATE TABLE menu_item (" +
                   " id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                   " nome TEXT not null, " +
                   " descricao TEXT not null, " +
                   " concentracao TEXT , " +
                   " ph TEXT , " +
                   " menu_id INTEGER not null) ");
    }
}

