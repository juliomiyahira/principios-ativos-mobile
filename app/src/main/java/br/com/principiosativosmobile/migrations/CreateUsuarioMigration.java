package br.com.principiosativosmobile.migrations;

import android.database.sqlite.SQLiteDatabase;

import se.emilsjolander.sprinkles.Migration;

/**
 * Created by julio on 25/09/15.
 */
public class CreateUsuarioMigration extends Migration {

    @Override
    protected void doMigration(SQLiteDatabase db) {
        db.execSQL(" CREATE TABLE usuario (" +
                   "  id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                   "  login TEXT not null , " +
                   "  senha TEXT not null, " +
                   "  nome TEXT not null) ");
    }
}
