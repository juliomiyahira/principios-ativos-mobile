package br.com.principiosativosmobile.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.google.android.gms.analytics.Tracker;

import br.com.principiosativosmobile.models.UserExpirationControllable;
import br.com.principiosativosmobile.models.Usuario;
import br.com.principiosativosmobile.utils.AnalyticsTracker;
import br.com.principiosativosmobile.utils.UserExpirationControl;

/**
 * Created by julio on 25/09/15.
 */
public class BaseActivity extends AppCompatActivity {

    public Tracker mTracker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mTracker = AnalyticsTracker.getTracker(this);
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (this instanceof UserExpirationControllable) {
            //Controle de duracao de sessao/autentenicacao/sessao
//            runUserExpirationControl();
        }
    }

    private void runUserExpirationControl() {

        UserExpirationControl expirationControl = UserExpirationControl.getInstance(getContentResolver());

        // -- Usuario nao esta autenticado
        if (expirationControl.userIsNotAuthenticated()) {
            forceExpireUserAndRedirectToLogin();
        }
    }

    private void forceExpireUserAndRedirectToLogin() {
        Usuario.logout();
        Intent intent = new Intent(this, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }
}