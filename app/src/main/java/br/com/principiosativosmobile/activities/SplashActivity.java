package br.com.principiosativosmobile.activities;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import br.com.principiosativosmobile.PrincipiosAtivosMobileApplication;
import br.com.principiosativosmobile.R;
import br.com.principiosativosmobile.utils.Constants;
import br.com.principiosativosmobile.utils.Dataload;

public class SplashActivity extends BaseActivity {

    private static final String SPLASH_EVENTO = "SPLASH_EVENTO";
    private static final String SPLASH_ACTION = "SPLASH_ACTION";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
    }

    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(receiver, new IntentFilter(SPLASH_EVENTO));
        new SyncControl().execute(this);
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);
        super.onPause();
    }

    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String evento = intent.getStringExtra(SPLASH_ACTION);
            if(SPLASH_ACTION.equals(evento)) {
                startActivity(new Intent(SplashActivity.this, NavigationActivity.class));
                finish();
            }
        }
    };

    class SyncControl extends AsyncTask<Activity, Void, Void> {
        @Override
        protected Void doInBackground(Activity... contexts) {
            try {
                loadData();
                Thread.sleep(4000);
                Intent intent = new Intent(SPLASH_EVENTO);
                intent.putExtra(SPLASH_ACTION, SPLASH_ACTION);
                LocalBroadcastManager.getInstance(contexts[0]).sendBroadcast(intent);
            } catch (InterruptedException ie) {
            }
            return null;
        }

        private void loadData() {
            SharedPreferences sharedPreferences = PrincipiosAtivosMobileApplication.getSharedPreferences();
            boolean isLoaded = sharedPreferences.getBoolean(Constants.SharedKey.FIRT_DATA_LOAD, false);
            if (!isLoaded) {
                Dataload.createMenu();
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putBoolean(Constants.SharedKey.FIRT_DATA_LOAD, true);
                editor.commit();
            }
        }
    }
}
