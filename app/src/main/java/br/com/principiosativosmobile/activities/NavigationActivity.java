package br.com.principiosativosmobile.activities;

import android.app.Notification;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.view.Menu;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;

import java.net.URI;
import java.net.URL;

import br.com.principiosativosmobile.BuildConfig;
import br.com.principiosativosmobile.R;
import br.com.principiosativosmobile.fragments.ActiveAboutFragment;
import br.com.principiosativosmobile.fragments.ActiveBibliografiaFragment;
import br.com.principiosativosmobile.fragments.ActiveSolicitarFormulaFragment;
import br.com.principiosativosmobile.fragments.ActivesCometicsFragment;
import br.com.principiosativosmobile.fragments.ActivesConsultingFragment;
import br.com.principiosativosmobile.models.UserExpirationControllable;
import br.com.principiosativosmobile.ui_utils.HelpLiveo;
import br.com.principiosativosmobile.ui_utils.MainFragment;
import br.com.principiosativosmobile.ui_utils.OnItemClickListener;
import br.com.principiosativosmobile.ui_utils.OnPrepareOptionsMenuLiveo;
import br.com.principiosativosmobile.utils.PaFragmentManager;

public class NavigationActivity extends NavigationLiveo implements OnItemClickListener, UserExpirationControllable {

    private static final String WEB_SITE_URL = "http://pacosmeticos.com.br";

    private HelpLiveo mHelpLiveo;

    private FragmentManager mFragmentManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mAdView != null) {
            mAdView.pause();
        }
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mAdView != null) {
            mAdView.resume();
        }
    }

    @Override
    public void onDestroy() {
        if (mAdView != null) {
            mAdView.destroy();
        }
        super.onDestroy();
    }

    @Override
    public void onInt(Bundle savedInstanceState) {
        this.userBackground.setImageResource(R.drawable.ic_user_background_pa);

        // Criação de itens de navegação
        mHelpLiveo = new HelpLiveo();
        mHelpLiveo.add(getString(R.string.menu_active_cosmetic), R.mipmap.ic_star_black_24dp);
        mHelpLiveo.addSeparator(); // Item separator
        mHelpLiveo.add(getString(R.string.menu_about_us), R.mipmap.ic_star_black_24dp);
        mHelpLiveo.addSeparator(); // Item separator
        mHelpLiveo.add(getString(R.string.menu_ref_bibliographic), R.mipmap.ic_star_black_24dp);
        mHelpLiveo.addSeparator(); // Item separator
        mHelpLiveo.add(getString(R.string.menu_consulting), R.mipmap.ic_star_black_24dp);
        mHelpLiveo.addSeparator(); // Item separator
        mHelpLiveo.add(getString(R.string.menu_request_formula), R.mipmap.ic_star_black_24dp);
        mHelpLiveo.addSeparator(); // Item separator
        mHelpLiveo.add(getString(R.string.link_for_web_site), R.mipmap.ic_star_black_24dp);


        with(this).startingPosition(0) //Starting position in the list
                .addAllHelpItem(mHelpLiveo.getHelp())
                .colorItemSelected(R.color.nliveo_blue_colorPrimary)
                .colorNameSubHeader(R.color.nliveo_blue_colorPrimary)
                .setOnPrepareOptionsMenu(onPrepare)
                .setOnClickFooter(onClickFooter)
                .build();

        int position = this.getCurrentPosition();
        this.setElevationToolBar(position != 2 ? 15 : 0);
    }

    @Override
    public void onItemClick(int position) {
        mFragmentManager = getSupportFragmentManager();
        switch (position) {
            case 0:
                PaFragmentManager.getInstance().setCurrentFragment(new ActivesCometicsFragment());
                break;

            case 2:
                PaFragmentManager.getInstance().setCurrentFragment(new ActiveAboutFragment());
                break;

            case 4:
                PaFragmentManager.getInstance().setCurrentFragment(new ActiveBibliografiaFragment());
                break;

            case 6:
                PaFragmentManager.getInstance().setCurrentFragment(new ActivesConsultingFragment());
                break;

            case 8:
                PaFragmentManager.getInstance().setCurrentFragment(new ActiveSolicitarFormulaFragment());
                break;

            case 10:
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(WEB_SITE_URL));
                startActivity(intent);
                break;

            default:
                PaFragmentManager.getInstance().setCurrentFragment(MainFragment.newInstance(mHelpLiveo.get(position).getName()));
                break;
        }

        if (PaFragmentManager.getInstance().getCurrentFragment() != null) {
            mFragmentManager.beginTransaction().replace(R.id.container, PaFragmentManager.getInstance().getCurrentFragment()).commit();
        }

        setElevationToolBar(position != 2 ? 15 : 0);
    }

    @Override
    public void onBackPressed() {
        PaFragmentManager.getInstance().getCurrentFragment().onBackPressedCallback();
    }

    private OnPrepareOptionsMenuLiveo onPrepare = new OnPrepareOptionsMenuLiveo() {
        @Override
        public void onPrepareOptionsMenu(Menu menu, int position, boolean visible) {
        }
    };

    private View.OnClickListener onClickPhoto = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Toast.makeText(getApplicationContext(), "onClickPhoto :D", Toast.LENGTH_SHORT).show();
            closeDrawer();
        }
    };

    private View.OnClickListener onClickFooter = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            startActivity(new Intent(getApplicationContext(), SettingsActivity.class));
            closeDrawer();
        }
    };
}

