package br.com.principiosativosmobile.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.Toast;

import br.com.principiosativosmobile.PrincipiosAtivosMobileApplication;
import br.com.principiosativosmobile.R;
import br.com.principiosativosmobile.exceptions.ApplicationException;
import br.com.principiosativosmobile.models.Usuario;
import br.com.principiosativosmobile.utils.Constants;
import br.com.principiosativosmobile.utils.Dataload;
import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class LoginActivity extends AppCompatActivity {

    @InjectView(R.id.login_value)
    public EditText login;

    @InjectView(R.id.password_value)
    public EditText senha;

    private void loadData() {
        SharedPreferences sharedPreferences = PrincipiosAtivosMobileApplication.getSharedPreferences();

        boolean isLoaded = sharedPreferences.getBoolean(Constants.SharedKey.FIRT_DATA_LOAD, false);
        if(!isLoaded) {
            Usuario usuario = new Usuario();
            usuario.login = "teste";
            usuario.senha = "teste";
            usuario.nome = "Usuário Teste";
            usuario.save();

            Dataload.createMenu();

            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putBoolean(Constants.SharedKey.FIRT_DATA_LOAD, true);
            editor.commit();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.inject(this);
    }

    @OnClick(R.id.entrar)
    public void entrar(){
        try {
            loadData();
            validar();
            Usuario usuario = Usuario.buscarPor(login.getText().toString(), senha.getText().toString());
            if(usuario != null) {
                Usuario.setUsuarioLogado(usuario);
                startActivity(new Intent(this, NavigationActivity.class));
                finish();
            }else {
                Toast.makeText(this, getString(R.string.login_and_password_invalid), Toast.LENGTH_SHORT).show();
            }
        }catch (ApplicationException ae) {
            Toast.makeText(this, getString(R.string.login_and_password_mandatory), Toast.LENGTH_SHORT).show();
        }
    }

    private void validar() throws ApplicationException{
        if(login.getText() == null || login.getText().toString().isEmpty() || senha.getText() == null || senha.getText().toString().isEmpty()) {
            throw new ApplicationException();
        }
    }
}
