package br.com.principiosativosmobile.models;

import java.util.List;

import se.emilsjolander.sprinkles.Model;
import se.emilsjolander.sprinkles.Query;
import se.emilsjolander.sprinkles.annotations.AutoIncrement;
import se.emilsjolander.sprinkles.annotations.Column;
import se.emilsjolander.sprinkles.annotations.Key;
import se.emilsjolander.sprinkles.annotations.Table;

/**
 * Created by julio on 25/09/15.
 */
@Table("menu")
public class Menu extends Model{

    @Key
    @AutoIncrement
    @Column("id")
    public long id;

    @Column("nome")
    public String nome;

    @Column("descricao")
    public String descricao;

    @Column("imageId")
    public int imageId;

    public static List<Menu> listarTodos() {
        return Query.many(Menu.class, " select * from menu").get().asList();
    }

    public static Menu buscarPorId(long menuId) {
        return Query.one(Menu.class, " select * from menu where id = ? ", menuId).get();
    }
}
