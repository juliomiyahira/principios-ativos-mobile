package br.com.principiosativosmobile.models;

import android.app.DownloadManager;

import se.emilsjolander.sprinkles.Model;
import se.emilsjolander.sprinkles.Query;
import se.emilsjolander.sprinkles.annotations.AutoIncrement;
import se.emilsjolander.sprinkles.annotations.Column;
import se.emilsjolander.sprinkles.annotations.Key;
import se.emilsjolander.sprinkles.annotations.Table;

/**
 * Created by julio on 25/09/15.
 */
@Table("usuario")
public class Usuario extends Model {

    @Key
    @AutoIncrement
    @Column("id")
    public long id;

    @Column("login")
    public String login;

    @Column("senha")
    public String senha;

    @Column("nome")
    public String nome;

    private static Usuario usuarioLogado;

    public Usuario() {
        super();
    }

    public Usuario(String login, String senha, String nome) {
        this.login = login;
        this.senha = senha;
        this.nome = nome;
    }

    public static Usuario getUsuarioLogado() {
        return usuarioLogado;
    }

    public static void setUsuarioLogado(Usuario usuario) {
        usuarioLogado = usuario;
    }

    public static void logout() {
        usuarioLogado = null;
    }

    public static Usuario buscarPor(String login, String senha) {
        return Query.one(Usuario.class, " select * from usuario where upper(login) = ? and upper(senha) = ? ", login.toUpperCase(), senha.toUpperCase()).get();
    }
}
