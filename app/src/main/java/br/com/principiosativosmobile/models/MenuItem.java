package br.com.principiosativosmobile.models;

import java.util.List;

import se.emilsjolander.sprinkles.Model;
import se.emilsjolander.sprinkles.Query;
import se.emilsjolander.sprinkles.annotations.AutoIncrement;
import se.emilsjolander.sprinkles.annotations.Column;
import se.emilsjolander.sprinkles.annotations.Key;
import se.emilsjolander.sprinkles.annotations.Table;

/**
 * Created by julio on 25/09/15.
 */
@Table("menu_item")
public class MenuItem extends Model {

    @Key
    @AutoIncrement
    @Column("id")
    public long id;

    @Column("nome")
    public String nome;

    @Column("descricao")
    public String descricao;

    @Column("concentracao")
    public String concentracao;

    @Column("ph")
    public String ph;

    @Column("menu_id")
    public long menuId;

    public static List<MenuItem> listarTodos() {
        return Query.many(MenuItem.class, "select * from menu_item").get().asList();
    }

    public static List<MenuItem> listarPorIdMenu(long menuId) {
        return Query.many(MenuItem.class, " select * from menu_item where menu_id = ? ", menuId).get().asList();
    }

    public static MenuItem findById(long menuItemId) {
        return Query.one(MenuItem.class, " select * from menu_item where id = ? ", menuItemId).get();
    }

    public static List<MenuItem> findByName(String string, long menuId) {
        return Query.many(MenuItem.class, " select * from menu_item where nome like ? and menu_id = ?", "%" + string + "%", menuId).get().asList();
    }
}
