package br.com.principiosativosmobile;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import java.util.Date;

import br.com.principiosativosmobile.migrations.CreateMenuItemMigration;
import br.com.principiosativosmobile.migrations.CreateMenuMigration;
import br.com.principiosativosmobile.migrations.CreateUsuarioMigration;
import br.com.principiosativosmobile.models.Menu;
import br.com.principiosativosmobile.models.MenuItem;
import br.com.principiosativosmobile.models.Usuario;
import br.com.principiosativosmobile.utils.Constants;
import br.com.principiosativosmobile.utils.Dataload;
import br.com.principiosativosmobile.utils.DateSerializerSprinkles;
import br.com.principiosativosmobile.utils.IntegerTypeSerializer;
import se.emilsjolander.sprinkles.Sprinkles;

/**
 * Created by julio on 25/09/15.
 */
public class PrincipiosAtivosMobileApplication extends Application {

    private static final String PREFERENCES = "session_preferences";
    private static Context sharedContext;
    public static String APP_TAG = "PRINCIPIOS_ATIVOS_MOBILE";
    public static String SHARED_PREF_APP= "SHARED_PREFF";

    public static String FREE_APPLICATION = "FREE_APPLICATION";
    public static String FULL_APPLICATION = "FULL_APPLICATION";


    public static boolean isFreeApp() {
        return BuildConfig.ENVIRONMENT.equals(FREE_APPLICATION);
    }

    public static boolean isFullApp() {
        return BuildConfig.ENVIRONMENT.equals(FULL_APPLICATION);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Sprinkles sprinkles = Sprinkles.init(getApplicationContext());
        sprinkles.registerType(Date.class, new DateSerializerSprinkles());
        sprinkles.registerType(Integer.class, new IntegerTypeSerializer());
        sprinkles.addMigration(new CreateUsuarioMigration());
        sprinkles.addMigration(new CreateMenuMigration());
        sprinkles.addMigration(new CreateMenuItemMigration());
        sharedContext = this;
    }

    public static SharedPreferences getSharedPreferences() {
        return sharedContext.getSharedPreferences(SHARED_PREF_APP, MODE_PRIVATE);
    }
}
