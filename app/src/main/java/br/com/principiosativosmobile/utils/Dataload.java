package br.com.principiosativosmobile.utils;

import android.content.SharedPreferences;

import br.com.principiosativosmobile.PrincipiosAtivosMobileApplication;
import br.com.principiosativosmobile.R;
import br.com.principiosativosmobile.models.Menu;
import br.com.principiosativosmobile.models.MenuItem;
import se.emilsjolander.sprinkles.annotations.Column;

/**
 * Created by kenji on 28/09/15.
 */
public class Dataload {

    public static long NUMBER_QUERY = 3l;

    public static void createMenu() {
        createMenuPaHidratantes();
        createMenuPaAntiacne();
        createMenuPaAnticelulite();
        createMenuPaDespigmentante();
        createMenuPaAntienvelecimento();
        createMenuPaFatoresCrescimento();
        createMenuPaExtratosVegetais();
    }

    public static boolean queryCountHasExpired() {
        SharedPreferences sharedPreference =  PrincipiosAtivosMobileApplication.getSharedPreferences();
        long currentCount = sharedPreference.getLong(Constants.SharedKey.NUMBER_QUERY, 0l);
        if(currentCount >= NUMBER_QUERY) {
            return true;
        }else {
            return false;
        }
    }

    public static void incrementQueryCount() {
        SharedPreferences sharedPreference =  PrincipiosAtivosMobileApplication.getSharedPreferences();
        long count = sharedPreference.getLong(Constants.SharedKey.NUMBER_QUERY, 0l);
        count++;
        SharedPreferences.Editor editor = sharedPreference.edit();
        editor.putLong(Constants.SharedKey.NUMBER_QUERY, count);
        editor.commit();
    }

    private static void createMenuPaExtratosVegetais() {
        Menu menu = new Menu();
        menu.nome = "Extratos Vegetais";
        menu.descricao = "Produtos Ativos Extratos Vegetais";
        menu.imageId = R.drawable.extratosvegetais;
        menu.save();
        insertMenuItem("Arnica Extrato (Glicólico)", "Adstringente, refrescante, antiinflamatório e cicatrizante. Nome científico: Arnica montana", "5 A 10%", "ampla Faixa", menu.id);
        insertMenuItem("Aloe Vera Extrato (Glicólico)", "Ação antiinflamatória, anti-séptica e regenerador epitelial (cicatrizante).","1 a 10%", "ampla faixa", menu.id);
        insertMenuItem("Babosa Extrato (Glicólico)", " Umectante, emoliente (usado para dar brilho em cabelos), vulnerário (próprio para curar feridas), refrescante, calmante (usado em produtos pós sol e pós peeling), cicatrizante em pequenas queimaduras. Nome científico: Aloe vera","2 A 10%", "ampla faixa", menu.id);
        insertMenuItem("Cavalinha Extrato (Glicólico)", " Adstringente e estimulantes da microcirculação. Nome científico: Equisetum arvense L.","5 A 10%", "ampla faixa", menu.id);
        insertMenuItem("Centella Asiática Extrato (Glicólico)", "Anti-trombótico, anti-varicoso, combate a celulite e tônico capilar, estimula a circulação periférica e revitalizante. Nome científico: Centella asiatica.","5 A 10%", "ampla Faixa", menu.id);
        insertMenuItem("Camomila Extrato (Glicólico)", "adstringente, anti-alérgico, antinflamatório, anti-pruriginoso, calmante, usado no pós-peeling. Nome científico: Matricaria chamomila","1 a 10%", "ampla faixa", menu.id);
        insertMenuItem("Castanha da Índia Extrato (Glicólico)", "Adstringente, anti-celulítico e estimulante da circulação periférica, tratamento de varizes. Nome científico: Aesculus hippocastanum ", "5 A 10%", "ampla Faixa", menu.id);
        insertMenuItem("Calêndula Extrato (Glicólico)", "Analgésico, refrescante, antiinflamatório, uso pós-peeling e anti-alérgico. Nome científico: Calendula officinalis", "2 a 10%", "ampla faixa", menu.id);
        insertMenuItem("Chá Verde Extrato (Glicólico)", "Antiinflamatório, antisséptico, bactericida, adstringente, antioxidante, antilipêmico, estimulante, melhora a circulação periférica. Nome científico: Camellia sinensis", "5 a 10%", "ampla Faixa", menu.id);
        insertMenuItem("Ginkgo Biloba Extrato (Glicólico)", "Estimulante da circulação periférica, nutritiva, revitalizante e antiqueda capilar.\nNome científico: Ginkgo biloba" , "5 A 10%", "Ampla Faixa", menu.id);
        insertMenuItem("Jaborandi Extrato (Glicólico)", "Cicatrizante, tônico, estimulante da circulação periférica.\nNome científico: Pilocarpus microphyllus" , "2 A 10%", "Ampla Faixa", menu.id);
        insertMenuItem("Hera Extrato (Glicólico)", "Função:adstringente, cicatrizante (acne), estimulante da circulação periférica, revitalizante.\nNome científico: Hedera helix" , "2 A 10%", "Ampla Faixa", menu.id);
        insertMenuItem("Hamamélis Extrato (Glicólico)", "Anti-séptico e adstringente (presença de taninos). Nome científico: Hamamelis virginiana" , "5 a 10%", "4,0 - 8,0", menu.id);
        insertMenuItem("Própolis Extrato (Glicólico)", "Antiséptico, adstringente, antiinflamatório, cicatrizante, antibacteriana e secativo." , "2 a 10%", "Ampla faixa", menu.id);
    }

    private static void createMenuPaFatoresCrescimento() {
        Menu menu = new Menu();
        menu.nome = "Fatores de Crescimento";
        menu.descricao = "Produtos Ativos Fatores de Crescimento";
        menu.imageId = R.drawable.fatoresdecrescimento;
        menu.save();
        insertMenuItem("AFGF – Fator de Crescimento Fibroblastico ácido", "Aumenta a elasticidade da pele através da indução da síntese de colágeno e elastina; Relacionado ao crescimento das células cutâneas e cura de ferimento; Promove o crescimento e inibe a despigmentação capilar; Estimula a circulação sanguínea do couro cabeludo, revitalizando os folículos capilares.", "1 a 3%", "5,0 - 7,0", menu.id);
        insertMenuItem("ƄFGF - Fator de Crescimento Fibroblastico básico", "Importante sinalizador para fibroblastos na síntese da matriz extracelular de boa qualidade, especialmente glicosaminoglicanas.","1 a 3%", "5,0 - 7,0", menu.id);
        insertMenuItem("EGF – Fator de Crescimento Epidermal", "Reduz e previne linhas e rugas pela ativação de novas células da pele; Devolve a uniformidade no tom da pele, vitalidade e energia;  Recupera a aparência jovial da pele Elimina cicatrizes e manchas da pele.","1 a 3%", "5,0 - 7,0", menu.id);
        insertMenuItem("IGF – Fator de Crescimento Insulínico", "Reduz e previne linhas e rugas através da ativação da geração de novas células cutâneas; Aumenta os níveis de colágeno e elastina da pele e reduz manchas avermelhadas; Possui um efeito redutor de gordura (drenagem) facial e corporal; Fortalece os cabelos enquanto estimula os folículos capilares a produzirem fios mais fortes.","1 a 3%", "5,0 - 7,0", menu.id);
        insertMenuItem("IDP - 2 Peptídeo (Decapeptideo 4)", "Favorece a cicatrização de úlceras de grandes extensões por reconstruir EPIDERME, DERME e HIPODERME, pelo mecanismo de promover o preenchimento de dentro para fora das três camadas da pele.","0,5 a 3%", "5,0 - 7,0", menu.id);
        insertMenuItem("Pró TG3", "Apresenta alta concentração de ácido eicosapentóico em sua composição, além disso, devido à presença das vitaminas C e E no composto, PRÓ-TG3® atua também como um potente agente antioxidante, aumentando sua eficácia anti-aging e reduzindo o processo inflamatório cutâneo. Composição: Ômegas 3, 6 e 9 oriundos da linhaça dourada + Vitamina C +Vitamina E.","a partir de 2%", "5,0 - 7,0", menu.id);
        insertMenuItem("TGP-2 Peptídeo - Decapeptídeo 34", "Citocina com propriedade despigmentante potente, considerado despigmentante bioidêntico. Seguro para todos os fototipos de pele. Não é fotossenssibilizante.", "2 a 5%", "5,0 - 7,0", menu.id);
        insertMenuItem("VEGF –Fator de Crescimento Vascular", "Estímulo do crescimento capilar; Facilitação da nutrição do folículo capilar; Indução da angiogênese.", "1 a 3%", "5,0 - 7,0", menu.id);
    }

    private static void createMenuPaAntienvelecimento() {
        Menu menu = new Menu();
        menu.nome = "Ativos Anti Envelhecimento";
        menu.descricao = "Produtos Ativos Anti Envelhecimento";
        menu.imageId = R.drawable.antienvelhecimento;
        menu.save();
        insertMenuItem("Ácido Alfa Lipóico", "Regenera a vit C e E; Protege DNA e mitocôndria da célula; Ajuda a remodelar o colágeno; Reduz a inflamação, bloqueando radicais livres", "1 a 5%", "4,0 - 6,5", menu.id);
        insertMenuItem("AA2G", "Liberação de vitamina C pura por ação da enzima alfa glucosidase. Ação clareadora, promove síntese de colágeno; antioxidante; atenuar e reduzir as linhas das rugas,", "2%", "5,0 - 7,0", menu.id);
        insertMenuItem("Ácido Hialurônico", "Capaz de reter água, formar filme elástico, proporciona elasticidade e tonicidade à pele. ","1 a 10%", "5,5 a 7,5", menu.id);
        insertMenuItem("Ácido Glicólico", "Tem sua função de acordo com a concentração e o pH. AHA. Diminui a adesão e coesão dos corneócitos, gera renovação celular e hidratação intensa.","2 a 10% - hidratante, despigmentante e esfoliante", "pH: 6,0 - torna-se glicolato, excelente hidratante , pH: 3,8 - esfoliante e despigmentante", menu.id);
        insertMenuItem("Algisium C", "Regeneração de fibroblastos e pidermico e ação antioxidante","2 a 6%", "4,5 - 6,5", menu.id);
        insertMenuItem("Ascorbosilane C", "Antioxidante potente, carreador de vitamina C.","2 a 6%", "4,5 - 6,5", menu.id);
        insertMenuItem("Argireline", "Prevene e reduz as linhas e rugas de expressão causadas por movimentos repetitivos (mais especificamente as rugas ao redor dos olhos, lábios, nariz e testa). Não altera a função dos músculos responsáveis pelos movimentos faciais, mantendo a naturalidade da expressão da face deixando a pele elástica. Também estimula a proliferação do número de fibroblastos.", "3 a 10%", "5,0 - 7,0", menu.id);
        insertMenuItem("Caviar HS", "Tem ações remineralizante e fortificante. Usado para produtos para pele madura, antienvelhecimento e pele ressecada", "1 a 5%", "6,0", menu.id);
        insertMenuItem("Coenzima Q-10", "Poderoso antioxidante, reduz o processo de envelhecimento orgânico.", "0,5 a 10%", "5,5 - 6,0", menu.id);
        insertMenuItem("Complexo ARL", "Composto por vitamina E, A, rutina e Ginkgo biloba. Ação antioxidante." , "1 a 6%", "6,0", menu.id);
        insertMenuItem("DMAE (N, N Dimetil Etanolamina, Deanol, 2-dimetil amino etanol)", "Precursor da síntese da acetilcolina: \nModulação da contração muscular do músculo liso da derme, aumento da contratilidade e adesão de outras células da derme e epiderme;\nAntioxidante e antiinflamatório: estabiliza as membranas celulares; \nProduz uma partição melhor de água através das membranas celulares e matriz dérmica; \nAumenta a retenção de água no tecido conectivo superficial; \nAumenta a firmeza da pele" , "3 a 10%", "3,5 - 7,0", menu.id);
        insertMenuItem("Drieline", "Estimulante do sistema imunológico da pele (glicosaminoglicanas). Estimula a síntese de colágeno, sendo agente regenerador e antienvelhecimento." , "0,1 a 10%", "6,0", menu.id);
        insertMenuItem("Elastinol +®", "Aumento da síntese de fibroblastos. Proporciona aumento da espessura  e do volume cutâneo, devolvendo uma aparência mais lisa, saudável e com menos ruga." , "1 a 5%", "5,5 - 6,5", menu.id);
        insertMenuItem("Elastocell", "Possui ação tensora, queratoplástica e hidratante. Atua nas rugas e manchas e aumenta a renovação celular e não é fotossensibilizante." , "5 a 10%", "6,0 - 8,0", menu.id);
        insertMenuItem("Endorphin®", "Ação neuroestimulante: aumenta a síntese de beta-endorfinas na pele.  Estas, promovem ação anti-estresse, relaxamento, sensação de bem-estar da pele.\nCompostos por polifenóis de cacau e por extrato da flor Tephrosia purpurea." , "3 a 8%", "5,5", menu.id);
        insertMenuItem("Ester C", "Antioxidante; anti radicais livres; proteção solar; manter a integridade da pele" , "0,15 a 2%", "6,0 - 8,0", menu.id);
        insertMenuItem("Epiderfil", "Microesferas de Ácido Hialurônico de baixo P.M. Preenchimento instantâneo das rugas. Penetra na pele e se reidrata, como se fosse uma esponja." , "5 a 10%", "5,0 - 6,0", menu.id);
        insertMenuItem("Eye Contour Complex", "Associação de ácido hialurônico, vitamina C, hidroxiprolina, fotesteróis de alcaçuz e cavaalinha, extratos de pimentão-doce, ruscus, grapefruit e proteínas. Atuam na permeabilidade dos vasos sanguíneos com ação antiedema, anti-radicais livres, regenerador, antiinflamatório, hidratante, despigmentante." , "4 a 10%", "Menor que 6,8", menu.id);
        insertMenuItem("Glicosfera de Vitamina C", "As glicosferas protegem a parte ativa da vitamina C, que estável, vai sendo liberada pelas camadas da pele, assegurando suas propriedades físico- químicas. Ação: antioxidante; clareadora; antiinflamatória; hidratante" , "1 a 5%", " 2,0 - 4,0", menu.id);
        insertMenuItem("Green Tea Extract", "Antioxidante, antienvelhecimento, antiinflamatório. Nome científico: Camellia sinensis" , "2 a 10%", "6,0", menu.id);
        insertMenuItem("Happybelle", "Fitoendorfinas nanotecnológicas: percursor de endorfina, melhorando a hidratação, suavização de rugas e sensação de bem-estar." , "1 a 2%", "3,0 - 8,0", menu.id);
        insertMenuItem("Hibiscus Vermelho", "Tem ação antioxidante, esfoliante suave e tônica." , "2 a 5%", "4,0 - 6,0", menu.id);
        insertMenuItem("Iris Iso®", "Inibe as atividades das enzimas colagenase e elastase, evitando a degradação das proteínas da matriz extracelular" , "3 a 5%", "5,0 - 10,0", menu.id);
        insertMenuItem("Kinetin L", "Sintetizado em Lipossomas PML®, estimulante de síntese de colágeno e elastina. Adenin ®, ácido alfa lipóico e VC-PMG®." , "5 a 10%", "4,8 - 8,0", menu.id);
        insertMenuItem("Lanablue", "Possui propriedades suavizantes da superfície da pele, é estimulante da reestruturação celular e age como ativo antienvelhecimento" , "1 a 10%", "5,0 - 7,0", menu.id);
        insertMenuItem("Lipossomas de Coezima Q10 (Ubiquinona)", "Atua inibindo a oxidação da membrana lipídica das células e estimulando o sistema imunológico da epiderme, deixando a pele com a aparência mais jovem e saudável." , "0,5 a 10%", "5,5 - 6,0", menu.id);
        insertMenuItem("Matrixil", "Polipeptídeo composto por lisina, treonina e serina. É um microcolágeno, que estimula a síntese de glicosaminoglicanas e ácido hialurônico." , " 5%", "5,0 - 7,0", menu.id);
        insertMenuItem("Nanomax®", "Antioxidante. VC-IP, Vitamina E e Coenzima Q10 em forma de nanopartículas." , "1 a 5%", "6,0 - 6,5", menu.id);
        insertMenuItem("Nanospheres de Vitamina C", "Liberação de ácido ascórbico puro de modo gradativo e em velocidade linear, atuando diretamente no grupamento S-S da cisteína da queratina da pele. Ação: Ativacao da biossíntese de colágeno; normalizacao da pigmentação cutânea; combater os radicais livres; aumentar a defesa da pele" , "0,5 a 5%", "5,0 - 7,0", menu.id);
        insertMenuItem("Nutripeptides", "Proteínas hidrolisadas do arroz. Estimula a síntese de colágenos III e VII e fibronectina pelos fibroblastos, além de estimular a proliferação dos mesmos. Aumenta a capacidade de recuperação dos fibroblastos submetidos à radiação UV-A e UV-B." , "1 a 4%", "2,0 - 10,0", menu.id);
        insertMenuItem("Phytosphingosine SLC", "Trata, minimiza e suaviza linhas de expressão facil da pele crono e foto-danificada. " , "0,05 a 0,2%", "5,0 - 7,0", menu.id);
        insertMenuItem("Raffermine", "Extrato da planta Glycine sp. Protege as fibras de elastina contra as enzimas elastases, que as  destroem causando flacidez." , "2 a 5%", "6,5", menu.id);
        insertMenuItem("Regu-Age", "É um complexo ativo constituído de soja especialmente purificada, peptídeos de arroz e proteína obtida de leveduras produzidas por biotecnologia. Melhora a hemodinâmica e a microcirculação sanguínea, reduz a quebra proteolítica da matriz de colágeno e elastina e reduz a presença de radicais livres. Reduz olheiras e bolsas ao redor dos olhos." , "2 a 5%", "5,5 - 8,0", menu.id);
        insertMenuItem("Thalasfera de Vitamina C", "Liberação de VC PMG por ação de enzimas da pele. Ação anti radicais livres; prevenção contra danos solares; prevenção do envelhecimento; clareadora" , "10%", "6,0 - 7,0", menu.id);
        insertMenuItem("Tensine", "Extraído das sementes de trigo. Tem ação tensora imediata, formando um filme altamente aderente, elástico, resistente e contínuo, capaz de diminuir o número e profundidade das rugas por algumas horas. \nTambém aumenta a durabilidade e permanência das maquiagens na pele. Seus efeitos são imediatos e, no máximo, serão notados após 1 hora de aplicação (especial para dias de festa)" , "3 a 10%", "6,5", menu.id);
        insertMenuItem("Vitamina C (Ácido Ascórbico)", "Participa como co-fator na hidroxilação da hidroxiprolina, importante aminoácido do tecido conjuntivo, melhorando elasticidade e firmeza da pele.\nAtua como inibidor da biossíntese melânica e portanto é utilizado como clareador de manchas na pele." , "0,5 a 10%", "5,5 - 6,0", menu.id);
        insertMenuItem("Vitamina E (Tocoferol)", "Antioxidante. Ação protetora no tecido cutâneo, doando elétrons diretamene para os radicais livres" , "0,2 a 2%", "5,5 - 6,5", menu.id);
        insertMenuItem("VC-PMG", "Ao penetrar na pele, enzimas atuam sobre a molécula de VC PMG liberando a vitamina C intacta. Ação clareadora das manchas; prevencao do envelhecimento" , "1 a 3%", "7,0 - 8,0", menu.id);
        insertMenuItem("VC-IP", "Após ser absorvido pela pele, o VC IP sofre ação enzimática, é clivado e origina a vitamina C livre. Ação: reducao dos efeitos do fotoenvelhecimento; tratamento da hiperpigmentação" , "0,05 a 10%", "4,0 - 6,0", menu.id);
    }

    private static void createMenuPaDespigmentante() {
        Menu menu = new Menu();
        menu.nome = "Ativos Despigmentantes";
        menu.descricao = "Produtos Ativos Despigmentantes";
        menu.imageId = R.drawable.despigmentante;
        menu.save();
        insertMenuItem("AA2G", "Liberação de vitamina C pura por ação da enzima alfa glucosidase. Ação clareadora, promove síntese de colágeno; antioxidante; atenuar e reduzir as linhas das ruga", "2%", "5,0 - 7,0", menu.id);
        insertMenuItem("Ácido Kójico", "Inibição da tirosinase. Quelam o cobre, substrato para ação da enzima.","1 a 3%", "3,0 - 5,0", menu.id);
        insertMenuItem("Ácido Fítico", "Inibição da tirosinase. Quelam o cobre, substrato para ação da enzima. Também tem ação antiinflamatória, antioxidante e hidratante.","0,5 a 2%", "4,0 - 4,5", menu.id);
        insertMenuItem("Ácido Glicirrízico", "Apresenta propriedades antiinflamatórias e antialérgicas, diminuindo também o efeito irritativo de outros ativos como ac. Glicólico e o ac. Retinóico.","0,1 a 0,2%", "3,0 - 4,0", menu.id);
        insertMenuItem("Ácido Glicólico", "Tem sua função de acordo com a concentração e o pH. AHA. Diminui a adesão e coesão dos corneócitos, gera renovação celular e hidratação intensa.","2 a 10% - hidratante, despigmentante e esfoliante", "pH: 6,0 - torna-se glicolato, excelente hidratante , pH: 3,8 - esfoliante e despigmentante", menu.id);
        insertMenuItem("Aqua Licorice Extract PT", "Extraído da raiz de Glycyrrhiza glabra lineé var., usado na prevenção e despigmentação de manchas induzidas pela alta exposição solar, atuando também como antienvelhecimento. É bacteriostático e possui atividade antimicrobiana contra S.aureus, S. epidermis, P.acnes, P.ovale.","0,5 a 1,0%", "5,0 - 7,0", menu.id);
        insertMenuItem("Algowhite", "Inibe a atividade da tirosinase e da proliferação dos melanócitos, apresenta ação antioxidante.", "2 a 5%", "ampla faixa", menu.id);
        insertMenuItem("Antipolon HT", "Adsorve a melanina já formada. Co – despigmentante. Sem efeito sensibilizante ou irritativo. Único despigmentante de ação física.", "1 a 5%", "4,0 - 8,0", menu.id);
        insertMenuItem("Arbutin (Beta)", "Inibição da tirosinase por ação direta.", "3 a 7% ou 0,5 a 4% quando associado.", "4,0 - 4,5", menu.id);
        insertMenuItem("Arbutin (Alpha)", "Inibição da tirosinase por ação direta. Mais estável e mais potente que o Arbutin Beta." , "0,2 a 2%", "3,5 - 6,5", menu.id);
        insertMenuItem("Azeloglicina", "Inibdor competitivo da tirosinase." , "5 a 10%", "5,5 - 6,5", menu.id);
        insertMenuItem("Belides", "Atua em diversas etapas da melanogênese: antes, durante e depois da formação de melanina. Obtido de flores de margarida (Bellis perennis)" , "2 a 5%", "5,5 - 6,5", menu.id);
        insertMenuItem("Biowhite", "Complexo de vegetais. \nExtrato de Morus nigra – ação antiinflamatória e inibidora da tirosinase leve; \nExtrato de Saxifraga stolonífera – ação anti-radicais livres, despigmentante e antiinflamatória; \nExtrato de Scutellaria baicalensis - ação antiinflamatória e inibidora da tirosinase leve; \nExtrato de Vitis vinifera – aumenta a penetração e potencializa a ação dos   outros ativos." , "1 a 4%", "6,5 - 7,5", menu.id);
        insertMenuItem("Cosmocair C250", "Impede a transferência de melanina dos melanócitos para os queratinócitos e inibe a atividade da tirosinase. Atua clareando a pele, não é irritante, pode ser usado durante o dia, não é citotóxico e não oxida facilmente" , "0,5 a 1,5%", "4,0 - 5,5", menu.id);
        insertMenuItem("Dermawhite", "Além da ação despigmentante, promove esfoliação moderada na pele" , "0,5 a 2%", "Abaixo de 5,0", menu.id);
        insertMenuItem("Glicosfera de Vitamina C", "As glicosferas protegem a parte ativa da vitamina C, que estável, vai sendo liberada pelas camadas da pele, assegurando suas propriedades físico- químicas.\nAção: antioxidante; clareadora; antiinflamatória; hidratante" , "1 a 5%", "2,0 - 4,0", menu.id);
        insertMenuItem("Glycosan Hidroquinona", "Preparado à base de hidroquinona protegido por ciclodextrinas. Evita problemas de sensibilização, que normamente ocorrem com a hidroquinona. Por ter liberação controlada, favorece o efeito despigmentante prolongado" , "5 a 10%", "4,0 - 5,5", menu.id);
        insertMenuItem("Hidroquinona", "Inibe a síntese de melanina e degrada as vesículas de armazenamento (Complexo Golgi) dos melanócitos" , "2 a 10%", "3,0 - 5,5", menu.id);
        insertMenuItem("Melawhite", "Atua inibindo a tirosinase no estágio inicial da melanogênese." , "2 a 5%", "4,0 - 5,5", menu.id);
        insertMenuItem("Melfade J", "Inibe a tirosinase e degrada a melanina já existente." , "3 a 8%", "4,0 - 5,5", menu.id);
        insertMenuItem("Nanospheres de Vitamina C", "Liberação de ácido ascórbico puro de modo gradativo e em velocidade linear, atuando diretamente no grupamento S-S da cisteína da queratina da pele. \nAção: Ativacao da biossíntese de colágeno; normalizacao da pigmentação cutânea; combater os radicais livres; aumentar a defesa da pele" , "0,5 a 5%", "5,0 - 7,0", menu.id);
        insertMenuItem("Skin Whitening Complex", "Inibe a formação da melanina, permite descoloração química do pigmento e retira as células. \nComplexo com Uva ursi, arroz e leveduras." , "2 a 5%", "4,0", menu.id);
        insertMenuItem("Thalasfera de Vitamina C", "Liberação de VC PMG por ação de enzimas da pele. \nAção anti radicais livres; prevenção contra danos solares; prevenção do envelhecimento; clareadora" , "10%", "6,0 - 7,0", menu.id);
        insertMenuItem("VC-PMG", "Ao penetrar na pele, enzimas atuam sobre a molécula de VC PMG liberando a vitamina C intacta. Ação clareadora das manchas; prevencao do envelhecimento" , "1 a 3%", "7,0 - 8,0", menu.id);
        insertMenuItem("VC-IP", "Após ser absorvido pela pele, o VC IP sofre ação enzimática, é clivado e origina a vitamina C livre. \nAção: reducao dos efeitos do fotoenvelhecimento; tratamento da hiperpigmentação" , "0,05 a 10%", "4,0 - 6,0", menu.id);
        insertMenuItem("Whitessence®", "Diminuição da transferência da melanina formada para os queratinócitos." , "2%", "6,0 - 8,0", menu.id);
    }

    private static void createMenuPaAnticelulite() {
        Menu menu = new Menu();
        menu.nome = "Ativos Anti Celulite";
        menu.descricao = "Produtos Ativos Anti Celulite";
        menu.imageId = R.drawable.celuliteegorduralocalizada;
        menu.save();
        insertMenuItem("Adipol", "Diminui a gordura localizada, e facilita a penetração dos outros ativos. Ação cicatrizante e lipolítica.", "4 a 6 %", "5,0 – 8,0", menu.id);
        insertMenuItem("Argisil c", "Ativa Lipólise (liberação de glicerol e ácidos graxos pelos adipócitos) através de receptores específicos localizados na membrana dos adipócitos. Previne a glicação, evitando formação de fibrose.","5%", "4,0 – 8,0", menu.id);
        insertMenuItem("AMARASHAPE", "Ativo nanotecnológico. Age diretamente sobre o metabolismo dos triglicerídeos nos adipócitos, promovendo a lipólise.","1 a 3%", "3,0 – 8,0", menu.id);
        insertMenuItem("Asiaticosídeo", "Ação normalizadora da circulação venosa de retorno, aumentando a elasticidade das paredes venosas, melhora a circulação sanguínea, elimina edemas e hematomas, combate processos degenerativos do tecido conjuntivo venoso.","0,1 a 0,5%", "Ampla Faixa", menu.id);
        insertMenuItem("Algisium C", "Regenera fibroblastos, combate a perda de elasticidade cutânea. Ação antiinflamatória e antiedema","4 a 6%", "4,5 - 6,5", menu.id);
        insertMenuItem("Arnica Extrato (glicólico)", "Adstringente, refrescante, antiinflamatório e cicatrizante. Nome científico: Arnica montana","5 a 10%", "Ampla Faixa", menu.id);
        insertMenuItem("Bioex anti-lipêmico", "Estimulante metabólico, ativador da microcirculação, antiinflamatório e anti-lipêmico. Ativo composto por extratos vegetais: arnica, castanha da índia, centella asiática, cavalinha, hera, fuccus.", "2 a 10 %", "3,5 – 7,0", menu.id);
        insertMenuItem("BIORUSOL", "Antiinflamatório, antiedematoso e vasoprotetor. Derivado da Rutina","0,5 a 1%","5,0 – 8,0", menu.id);
        insertMenuItem("Cafeína", "Melhora a circulação sanguínea, causando vasodilatação", "até 8 % ", "5,0– 8,0", menu.id);
        insertMenuItem("Celulinol", "Complexo de polietilenoglicol e salicilato de amila que auxiliam na prevenção da celulite. Ação antiinflamatória e Ação lipolítica: diminui gordura localizada", "4 a 6 %", "5,0 – 8,0", menu.id);
        insertMenuItem("Cafeisilane C®", "Repositor de silício endógeno, estimula a síntese de fibras de sustentação da pele, ativa a hidrólise de triglicérides,  o acúmulo de lipídeos e gera vasodilatação", "3 a 6 %", "3,5 a 7,0", menu.id);
        insertMenuItem("Cânfora", "Ação rubefaciente – aumenta a atividade vascular local, podendo provocar a vermelhidão. Ação analgésica suave, antipruriginosa e anti-séptica." , "1 a 5 %", "3,5  a 7,0", menu.id);
        insertMenuItem("CAFEISKIN ®", "Composto de cafeína, extrato de Centella asiática, extrato de Ginkgo biloba e extrato de castanha da índia. Vasodilatador." , "3 a 10%", "5,5 – 6,5", menu.id);
        insertMenuItem("Nicotinato de metila e Salicilato de metila", "Ação rubefaciente – aumenta a atividade vascular local, podendo provocar a vermelhidão e antiinflamatório. Facilita a absorção de outros ativos." , "0,1 a 0,5 %", "3,5  - 6,0", menu.id);
        insertMenuItem("Cavalinha Extrato (Glicólico)", "Adstringente e estimulantes da microcirculação. Nome científico: Equisetum arvense L." , "5 A 10%", "Ampla Faixa", menu.id);
        insertMenuItem("Centella Asiática Extrato (Glicólico)", "Anti-trombótico, anti-varicoso, combate a celulite e tônico capilar, estimula a circulação periférica e revitalizante. Nome científico: Centella asiatica" , "5 A 10%", "Ampla Faixa", menu.id);
        insertMenuItem("Castanha da Índia Extrato (Glicólico)", "Adstringente, anti-celulítico e estimulante da circulação periférica, tratamento de varizes. Nome científico: Aesculus hippocastanum" , "5 A 10%", "Ampla Faixa", menu.id);
        insertMenuItem("Chá Verde Extrato (Glicólico)", "Antiinflamatório, antisséptico, bactericida, adstringente, antioxidante, antilipêmico, estimulante, melhora a circulação periférica. Nome científico: Camellia sinensis" , "5 A 10%", "Ampla Faixa", menu.id);
        insertMenuItem("Ginkgo Biloba Extrato (Glicólico)", "Estimulante da circulação periférica, nutritiva, vasoprotetor, revitalizante e antiqueda capilar.\nNome científico: Ginkgo biloba" , "5 A 10%", "Ampla Faixa", menu.id);
        insertMenuItem("Hera Extrato (Glicólico)", "Função:adstringente, cicatrizante (acne), estimulante da circulação periférica, revitalizante. Nome científico: Hedera helix" , "2 A 10%", "Ampla Faixa", menu.id);
        insertMenuItem("IS0-SLIM COMPLEX ®", "Inibe a formação de novos adipócitos, promove a quebra da gordura e também aumenta fortemente a resistência, firmeza e aparência lisa da pele. \nConstituído por Genisteína, Cafeína e Carnitina" , "3 a 5%", "3,5 - 7,0", menu.id);
        insertMenuItem("Liporeductil ®", "Inibição da formação de adipócitos, ação lipolítica e ativadora da microcirculação.  Ativo lipossomado composto por cafeína, Ruscus aculeatus, compostos iodados, Hedera helix, carnitina, escina  e glicil-histidil-lisina" , "5 a 10%", "4,8 - 8,0", menu.id);
        insertMenuItem("LIPOLYSSE ®", "Atua na lipólise – Reduz a gordura localizada; Estimula produção de colágeno – Firma e regenera a pele; Combate o estresse oxidativo – Anti-Radicais Livres; Normalização da micro circulação - Ativa a circulação local. Cafeína, Asiaticosídeo, Café Verde, L-Carnitina" , "3 a 10%", "3,5 - 7,0", menu.id);
        insertMenuItem("Lipoxyn", "Atua esvaziando os adipócitos (lipólise) e atua combatendo sua replicação (anti lisogênico). \nCombate gordura localizada." , "5 a 7%", "3,5 - 7,0", menu.id);
        insertMenuItem("Mentol", "Topicamente age sobre a pele, dilatando os vasos sanguíneos, causando sensação de frio e analgesia" , "0,25 a 1 %", "3,5 - 7,0", menu.id);
        insertMenuItem("Nicotinato de Metila", "Agente hiperêmico para aplicação tópica. Rubefaciente, revulsivo, vasodilatador, aumenta a atividade trofovascular, anntiinflamatório." , "0,05 a 0,2%", "5,0 - 6,0", menu.id);
        insertMenuItem("Remoduline ®", "Ativo lipolítico que possui ação descongestionante e drenante, estimula a lipólise, ativa a microcirculação, elimina a estase venosa e o edema. Extrato de laranja amarga." , "2 a 5%", "2,0 - 7,0", menu.id);
        insertMenuItem("Regu-Slim", "Deflagra o processo de lipólise, estimulando a quebra de trigliérides no interior dos adipócitos, facilitando o seu transporte até a mitocôndria e aumentando sua oxigenação no interior da mesma. Reduz a espessura do tecido adiposo e melhora o aspecto da pele tipo \"casca de laranja\" " , "2%", "5,0 - 6,0", menu.id);
        insertMenuItem("Rutiderm", "Atua como carreador, aumentando a permeabilidade transepidérmica. Tem propriedades antiinflamatória, antiedematosa e vasoprotetora, capazes de diminuir a permeabilidade vascular da pele inflamada e de promover a reabsorção de edemas" , "1 a 3%", "Acima de 5,0", menu.id);
        insertMenuItem("Slimbuster® L", "Ação simultânea na redução de gordura localizada por aumentar os níveis de leptina e confere ação regeneradora tecidual.\nÓleo da semente de café" , "3%", "4,0 - 7,0", menu.id);
        insertMenuItem("Slimbuster® H", "Melhora o fluxo sanguíneo com redução da retenção de líquidos. Produz hidrólise da gordura armazenada nos adipócitos. \nMarapuama, Catuaba e Pfaffia" , "5%", "4,0 - 7,0", menu.id);
        insertMenuItem("Theophyllisilane C ®", "Repositor de silício endógeno, ativa a hidrólise de triglicerídeos (ação ativadora da lipólise)." , "2 a 6%", "3,5 - 7,0", menu.id);
        insertMenuItem("Thiomucase", "Condrointinuslfatase. Constitui-se numa mucopolisacaridase, ou seja, é uma enzima biológica extraída do testículo e sêmen de bovinos, que despolimeriza as glicosaminoglicanas, atuando sobre o ácido hialurônico e principalmente sobre o ácido condroitinossulfúrico, principais componentes do cemento intercelular" , "250 a 20000UTR%", "5,5 - 6,0", menu.id);
        insertMenuItem("Xantalgosil C®", "Lipolítico com simultânea ação firmadora. Silanol (Acefilina e Ácido Algínico)" , "3 a 6%", "3,5 - 6,5", menu.id);
    }

    private static void createMenuPaAntiacne() {
        Menu menu = new Menu();
        menu.nome = "Ativos Anti Acne";
        menu.descricao = "Produtos Ativos Anti Acne";
        menu.imageId = R.drawable.antiacne;
        menu.save();

        insertMenuItem("Ácido Salicílico", "Beta-hidroxiácido, comedolítico e queratolítico , promove descamação. Ação bacteriostática e fungicida.", "Concentração: 0,5 – 2% (Face)- Até 5% costas e couro cabeludo - Até 20  % para retirada de verrugas e calosidade", "3,0 - 3,5", menu.id);
        insertMenuItem("Ácido Glicirrízico", "Anti-inflamatório, reduz eritema e prurido","0,1 a 0,3%", "3,0 - 4,0", menu.id);
        insertMenuItem("Alantoína", "Tem ação estimulante da proliferação celular e ativador da cicatrização.","0,2 a 2%", "6,0", menu.id);
        insertMenuItem("Asebiol", "Ação adstringente, regulador da secreção e antiinflamatório. Não provoca efeito rebote.","3 a 5%", "5,5 - 6,0", menu.id);
        insertMenuItem("Acqua Licorice Extract PU", "Ação seborreguladora, inibe a enzima α-5 redutase. Ação antimicrobiana contra P. acnes","0,5 a 1%", "5,0 - 7,0", menu.id);
        insertMenuItem("AC NET", "Reduz a seborréia, hiperqueratose, inflamação e proliferação bacteriana","2 a 4%", "5,0 - 6,0", menu.id);
        insertMenuItem("Alfa Bisabolol", "Ação antiinflamatória, cicatrizante e anti-séptica.","0,1 a 1%", "4,5 - 6,0", menu.id);
        insertMenuItem("Aloe Vera Extrato (Glicólico)", "Ação antiinflamatória, anti-séptica e regenerador epitelial (cicatrizante).","1 a 10%", "ampla faixa", menu.id);
        insertMenuItem("Antiacne Complex", "Ajuda a normalizar a queratogênese e a secreção seborréica. Ação antiinflamatória e anti-séptica","4 a 10%", "5,5", menu.id);
        insertMenuItem("Anti-oil Spheres (Microesferas de sílica)", "Adsorve a oleosidade da pele","1 a 20%", "5,0 - 7,0", menu.id);
        insertMenuItem("Arnica Extrato (Glicólico)", "Adstringente, refrescante, antiinflamatório e cicatrizante. Nome científico: Arnica montana","5 a 10%", "4,0 - 8,0", menu.id);
        insertMenuItem("Azeloglicinia", "Inibe a enzima 5 alfa redutase e reduz a oleosidade da pele, despigmentante, antiinflamatório, antibacteriana, esfoliante", "5 a 10%", "6,5 - 7,0", menu.id);
        insertMenuItem("Azuleno", "Ação antiinflamatório (obtido da Camomila) e cicatrizante","0,01 a 0,03%", "5,0 - 6,0", menu.id);
        insertMenuItem("Biossulfur Fluid", "Queratolítico e antiinflamatório ","0,1 a 2%", "5,5", menu.id);
        insertMenuItem("Bioex antioleosidade", "Complexo de extratos vegetais. Adstringente, antiséptico, antioxidante, cicatrizante, anticaspa, antiseborreico, antiinflamatório.","1 a 5% (xampus e tônicos capilares); 2 a 10% (face)", "Ampla faixa", menu.id);
        insertMenuItem("Camomila Extrato (Glicólico)", "Adstringente, anti-alérgico, antinflamatório, anti-pruriginoso, calmante, usado no pós-peeling. Nome científico: Matricaria chamomila","1 a 10%", "Ampla faixa", menu.id);
        insertMenuItem("Calêndula Extrato (Glicólico)", "Analgésico, refrescante, antiinflamatório, uso pós-peeling e anti-alérgico. \nNome científico: Calendula officinalis","2 a 10%", "Ampla faixa", menu.id);
        insertMenuItem("Glycosan Copaíba", "Ativo de liberação progressiva com efeito cicatrizante e anti-séptico","1 a 5%", "5,5", menu.id);
        insertMenuItem("Glycosan Salicílico", "Ativo de liberação progressiva com efeito queratolítico e antiacne","1 a 5%", "5,5", menu.id);
        insertMenuItem("Hamamélis Extrato (Glicólico)", "Anti-séptico e adstringente (presença de taninos). \nNome científico: Hamamelis virginiana","5 a 10%", "Ampla faixa", menu.id);
        insertMenuItem("Irgasan ou Triclosan", "Antibacteriano e Antiséptico","0,1 a 0,5%", "4,0 - 8,0", menu.id);
        insertMenuItem("Lipaceide C8G", "Reduz o número de escamas causada por P. ovale; reduz irritação, inibe a 5 alfa-redutase. Hipoalergênico","0,5 a 2%", "5,0 - 7,0", menu.id);
        insertMenuItem("Microesferas de Polietileno", "Esfoliante físico","0,5 a 1%", "Ampla faixa", menu.id);
        insertMenuItem("Óleo de Copaíba", "Antibacteriano","1 a 5%", "4,0 - 9,0", menu.id);
        insertMenuItem("Óleo de Melaleuca", "Ação antifúngica, anti-bactericida, anti-seborreico","1 a 10%", "4,0 - 9,0", menu.id);
        insertMenuItem("Peróxido de Benzoíla", "Queratolítico, antibacteriano e anti-seborréica","2 a 10%", "5,0 - 6,0", menu.id);
        insertMenuItem("Própolis Extrato (Glicólico)", "Antiséptico, adstringente, antiinflamatório, cicatrizante, antibacteriana e secativo","2 a 10%", "Ampla faixa", menu.id);
        insertMenuItem("Phytosphingosine", "Antimicrobiano e antiinflamatório","0,05 a 0,2%", "5,5 - 6,5", menu.id);
        insertMenuItem("Polytrap 6603®", "Absorve oleosidade (esferas - microesponjas)","2 a 4%", "Ampla faixa", menu.id);
        insertMenuItem("Regu-Seb®", "Inibe 5 alfa redutase","2 a 5%", "5,0 - 8,0", menu.id);
        insertMenuItem("Sebonormine", "Inibe a enzima 5 alfa redutase, normalizando a secreção sebácea","2 a 5%", "4,0 a 10,0", menu.id);
        insertMenuItem("Sementes de Apricot", "Esfoliante físico","1 a 10%", "ampla faixa", menu.id);
        insertMenuItem("Sepicontrol A5", "Regula produção sebácea por inibir a enzima 5 alfa redutase, antimicrobiano, antiseborreico","4%", "5,0 - 7,0", menu.id);
        insertMenuItem("Tiolisina Complex 30", "Sebo-normalizante usado para cabelos e pele","3 a 5%", "6,0 - 8,0", menu.id);
        insertMenuItem("Zinco (seus sais): acetato, sulfato, glucolato e pidolato - zincidone", "Antibacteriano e Antiinflamatório","1 a 10%", "5,0 - 6,5", menu.id);
    }

    private static void createMenuPaHidratantes() {
        Menu menu = new Menu();
        menu.nome = "Ativos Hidratantes";
        menu.descricao = "Produtos Ativos Hidratantes";
        menu.imageId = R.drawable.ativoshidratantes;
        menu.save();

        insertMenuItem("Ácido Hialurônico", "Capaz de reter água, formar filme elástico, proporciona elasticidade e tonicidade à pele.", "1 a 10%", "5,5 a 7,5", menu.id);
        insertMenuItem("Ácido Glicólico", "Tem sua função de acordo com a concentração e o pH. AHA. Diminui a adesão e coesão dos corneócitos, gera renovação celular e hidratação intensa.","2 a 10% - hidratante, despigmentante e esfoliante","pH: 6,0 - torna-se glicolato, excelente hidratante, pH: 3,8 - esfoliante e despigmentante", menu.id);
        insertMenuItem("Amiporine", "Extraído da Romã, gera o aumento da atividade das aquaporinas.","2 a 4%","5,0 - 6,0", menu.id);
        insertMenuItem("Aquaporine active", "Proporciona o aumento da hidratação da epiderme por aumentar a atividade das aquaporinas da membrana.","2 a 5%", "5,0 - 6,0", menu.id);
        insertMenuItem("Avenolat II", "Oclusão e umectação, por reposição de lipídeos e proteínas hidrolisadas de aveia. Indicado para peles sensíveis.","1 a 5%", "5,0 - 6,5", menu.id);
        insertMenuItem("Active-Aloe", "Polpa de Aloe vera. Ação hidratante, regenerador, ajuda na penetração dos ativos.","0,05 a 0,5%", "6,0", menu.id);
        insertMenuItem("Alantoína", "Tem ação estimulante da proliferação celular e ativador da cicatrização. Tem ação hidratante","0,2 a 2%", "6,0", menu.id);
        insertMenuItem("Alistin", " Recupera a epiderme submetida à radiação UV. Ação prolongada.","0,5 a 1,5%", "3,0 -7,0", menu.id);
        insertMenuItem("Biolift H", "Preserva a epiderme, mantém a estrutura cutânea, estimula formação de colágeno, fibroblastos e queratinócitos.","2 a 7%", "6,0", menu.id);
        insertMenuItem("Caviar HS", "Umectação. Obtido das ovas de esturjão, contendo vitaminas, aminoácidos e sais minerais, todos hidrossolúveis","1 a 5%", "5,0 - 6,0", menu.id);
        insertMenuItem("Caviar LS®", "Oclusão. Obtido das ovas de esturjão, contendo vitamina A e D e ácidos graxos importantes, todos lipossolúveis.","1 a 5%", "5,0 - 6,0", menu.id);
        insertMenuItem("Colágeno", "Hidratação advém da formação de filme sobre a pele","2 a 10%", "5,5 - 6,0", menu.id);
        insertMenuItem("Ceramidas", "Grupo de lipídeos complexos formados na pele, papel essencial como barreira da pele,  nos cabelos: une as microescamas dos cabelos, restaurando os fios e evitando o ressecamento e as pontas duplas","0,05 – 1 %", "5,0 – 7,0", menu.id);
        insertMenuItem("Ciclometicone Volátil", "Silicone de baixa viscosidade. Lubrificante com toque seco, aveludado e de ação prolongada.","1 a 10%", "3,0 - 8,0", menu.id);
        insertMenuItem("Codiavelane", "Promove hidratação nos mucopolissacarídeos da pele.","2 a 5%", "5,5 - 6,5", menu.id);
        insertMenuItem("D-Pantenol", "Pró- vitamina B5, ação hidratante, antiinflamatória e estimulante da epitelização","0,1 a 2%", "6,0", menu.id);
        insertMenuItem("Densiskin", "Age na matriz extracelular, aumentando a densidade da pele. Hidratante e umectante.","1 a 6%", "4,0 - 6,5", menu.id);
        insertMenuItem("Dimeticone (Fluido de Silicone)", "Forma filme protetor e emoliente, formando barreira oclusiva. Usado em face, corpo e cabelo.","1 a 10%", "3,0 - 8,0", menu.id);
        insertMenuItem("Elastina", "Hidratação advém da formação de filme sobre a pele","1 a 3%", "5,5 - 6,0", menu.id);
        insertMenuItem("Enteline 2", "Hidratante pré e pós sol. Reduz resposta inflamatória do eritema, prurido e descamações de exposição a radiação UV","2 a 3%", "5,5 - 6,5", menu.id);
        insertMenuItem("Fomblin HC", "Forma filme, mas não obstrui poros. Repelente à água, não oleoso, não comedogênico e não irritante.","0,1 a 3%", "Ampla faixa", menu.id);
        insertMenuItem("Fucogel", "Polissacarídeo de grande poder de hidratação, formador de filme. Minimiza irritação na pele causada pelos AHA´S","0,1 a 5%", "3,0 - 10,0", menu.id);
        insertMenuItem("Hidroviton", "Contém uréia e alantoína, fazendo parte do fator de hidratação natural (NFM)","1 a 5%", "4,0 - 5,5", menu.id);
        insertMenuItem("Hyaloporine", "Ativa a expressão da hidratação biológica","2 a 5%", "5,0 - 6,0", menu.id);
        insertMenuItem("Lactato de amônia", "Especialmente indicado para peles secas","12 a 20 %", "4,5 - 7,0, \n acima do pH : 7,0 ocorre liberação de amônia, liberando um odor característico.", menu.id);
        insertMenuItem("Lipobelle Royal", "Estimula a produção de colágeno e elastina na derme, promovendo aumento da densidade, firmeza e elasticidade cutânea","1 a 5 %", "5,0 -7,0", menu.id);
        insertMenuItem("Lipossomas de Ceramidas", "Barreira cutâne e hidrorregulação cutânea","2 a 10 %", "5,0 -8,0", menu.id);
        insertMenuItem("Manteiga de Karitê", "Excelente emoliente com propriedades antiirritantes.","0,5 a 10%", "5,0 - 6,0", menu.id);
        insertMenuItem("Óleo de Macadâmia", "Emoliente e hidratante por oclusão","1 a 10%", "Ampla faixa", menu.id);
        insertMenuItem("Óleo de Açaí", "Emoliente e hidratante por oclusão","1 a 10%", "Ampla faixa", menu.id);
        insertMenuItem("Óleo de Amendôa Doce", "Emoliente e hidratante por oclusão","1 a 10%", "Ampla faixa", menu.id);
        insertMenuItem("PCA-NA", "Hidratante por umectação","1 a 5%", "4,0 - 6,5", menu.id);
        insertMenuItem("Sensiline", "Extraído da semente da linhaça. Hidrata a pele, preservando o sistema imunológico e diminui inflamação","2 a 7%", "acima de 6,0", menu.id);
        insertMenuItem("SK Influx", "Repõem perdas lipidicas do extrato córneo, protege e hidrata a pele.","1.5% a 5% para pele normal; 3% a 5% para pele seca e envelhecida;\n3% a 15% para proteção da pele.", "5,0 - 7,0", menu.id);
        insertMenuItem("Uréia", "É um dos ativos hidratantes corporais mais utilizados, tendo uma ação queratolítica em concentrações acima de 10%","3 a 10%", "5,0 - 6,0", menu.id);
        insertMenuItem("Vitamina E", "Ação anti-radicais livres e hidratantes","0,1 a 5%", "3,5 - 8,0", menu.id);
    }

//  insertMenuItem("", "","", "", menu.id);
    private static void insertMenuItem(String nome, String descricao, String concentracao, String ph, long menuId) {
        MenuItem menuItem = new MenuItem();
        menuItem.nome = nome;
        menuItem.descricao = descricao;
        menuItem.concentracao = concentracao;
        menuItem.ph = ph;
        menuItem.menuId = menuId;
        menuItem.save();
    }
}
