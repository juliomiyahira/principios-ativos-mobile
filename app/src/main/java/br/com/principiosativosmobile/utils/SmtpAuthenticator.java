package br.com.principiosativosmobile.utils;

import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;

/**
 * Created by julio on 22/10/15.
 */
public class SmtpAuthenticator extends Authenticator {

    private String username;
    private String password;

    public SmtpAuthenticator(String username, String password) {
        super();
        this.username = username;
        this.password = password;
    }

    @Override
    public PasswordAuthentication getPasswordAuthentication() {
        if ((username != null) && (username.length() > 0) && (password != null) && (password.length() > 0)) {
            return new PasswordAuthentication(username, password);
        }
        return null;
    }
}

