package br.com.principiosativosmobile.utils;

/**
 * Created by julio on 25/09/15.
 */
public class Constants {

    public static class SharedKey {
        public static String FIRT_DATA_LOAD = "FIRT_DATA_LOAD";
        public static String NUMBER_QUERY= "NUMBER_QUERY";
    }

    public static class Extra {
        public static final String MENU_ITEM_ID = "MENU_ITEM_ID";
        public static final String MENU_ID = "MENU_ID";
    }
}
