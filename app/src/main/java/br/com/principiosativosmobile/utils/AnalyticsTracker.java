package br.com.principiosativosmobile.utils;

import android.content.Context;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;

import br.com.principiosativosmobile.R;

/**
 * Created by julio on 25/09/15.
 */
public class AnalyticsTracker {

    private static Tracker mTracker;

    public static Tracker getTracker(Context context) {
        if (mTracker == null) {
            GoogleAnalytics analytics = GoogleAnalytics.getInstance(context);
            mTracker = analytics.newTracker(R.xml.analytics_tracker);
        }
        return mTracker;
    }
}
