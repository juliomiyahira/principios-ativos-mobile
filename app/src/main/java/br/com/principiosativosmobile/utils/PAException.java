package br.com.principiosativosmobile.utils;

/**
 * Created by kenji on 01/11/15.
 */
public class PAException extends Exception {

    public PAException(String detailMessage) {
        super(detailMessage);
    }
}
