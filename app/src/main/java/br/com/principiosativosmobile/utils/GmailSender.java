package br.com.principiosativosmobile.utils;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.Security;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 * Created by julio on 22/10/15.
 */
public class GmailSender {

    private static final String MAIL_HOST = "smtp.gmail.com";

    public static final String user = "contato.pacosmeticos@gmail.com";

    private static final String password = "pacosmeticos50";

    private Properties emailProperties;

    static {
        Security.addProvider(new JSSEProvider());
    }

    public GmailSender() {
        emailProperties = new Properties();
        emailProperties.setProperty("mail.transport.protocol", "smtp");
        emailProperties.setProperty("mail.host", MAIL_HOST);
        emailProperties.put("mail.smtp.auth", "true");
        emailProperties.put("mail.smtp.port", "465");
        emailProperties.put("mail.smtp.socketFactory.port", "465");
        emailProperties.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        emailProperties.put("mail.smtp.socketFactory.fallback", "false");
        emailProperties.setProperty("mail.smtp.quitwait", "false");
    }

    public synchronized void sendMail(String titulo, String conteudo, String emailOrigin, String emailDestino) throws Exception {
        try {
            MimeMessage message = new MimeMessage(Session.getDefaultInstance(emailProperties, new SmtpAuthenticator(user, password)));
            DataHandler handler = new DataHandler(new ByteArrayDataSource(conteudo.getBytes(), "text/plain"));
            message.setSender(new InternetAddress(emailOrigin));
            message.setSubject(titulo);
            message.setDataHandler(handler);
            if (emailDestino.indexOf(',') > 0) {
                message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(emailDestino));
            } else {
                message.setRecipient(Message.RecipientType.TO, new InternetAddress(emailDestino));
                Transport.send(message);
            }
        } catch (Exception e) {
            throw e;
        }
    }

    public class ByteArrayDataSource implements DataSource {
        private byte[] data;
        private String type;

        public ByteArrayDataSource(byte[] data, String type) {
            super();
            this.data = data;
            this.type = type;
        }

        public ByteArrayDataSource(byte[] data) {
            super();
            this.data = data;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getContentType() {
            if (type == null)
                return "application/octet-stream";
            else
                return type;
        }

        public InputStream getInputStream() throws IOException {
            return new ByteArrayInputStream(data);
        }

        public String getName() {
            return "ByteArrayDataSource";
        }

        public OutputStream getOutputStream() throws IOException {
            throw new IOException("Not Supported");
        }
    }
}
