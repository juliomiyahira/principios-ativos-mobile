package br.com.principiosativosmobile.utils;

import br.com.principiosativosmobile.fragments.BaseFragment;

/**
 * Created by julio on 20/10/15.
 */
public class PaFragmentManager {

    private static PaFragmentManager fragmentManager;

    private BaseFragment fragment;

    private PaFragmentManager() {
        super();
    }

    public static PaFragmentManager getInstance() {
        if (fragmentManager == null) {
            fragmentManager = new PaFragmentManager();
        }
        return fragmentManager;
    }

    public BaseFragment getCurrentFragment() {
        return fragment;
    }

    public void setCurrentFragment(BaseFragment fragment) {
        this.fragment = fragment;
    }
}
