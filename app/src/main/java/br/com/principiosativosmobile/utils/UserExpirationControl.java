package br.com.principiosativosmobile.utils;

import android.content.ContentResolver;

import br.com.principiosativosmobile.models.Usuario;

/**
 * Created by julio on 25/09/15.
 */
public class UserExpirationControl {

    private static long MINUTES_IDLE_TIME = 30;

    private static UserExpirationControl userExpirationControl;

    private ContentResolver contentResolver;

    public static UserExpirationControl getInstance(ContentResolver contentResolver) {
        if (userExpirationControl == null) {
            userExpirationControl = new UserExpirationControl(contentResolver);
        }
        return userExpirationControl;
    }

    private UserExpirationControl(ContentResolver contentResolver) {
        super();
        this.contentResolver = contentResolver;
    }

    public boolean userIsNotAuthenticated() {
        Usuario usuario = Usuario.getUsuarioLogado();
        if(usuario == null) {
            return true;
        }
        return false;
    }
}
