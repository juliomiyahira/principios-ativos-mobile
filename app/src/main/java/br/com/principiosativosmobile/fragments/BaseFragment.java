package br.com.principiosativosmobile.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;

import br.com.principiosativosmobile.R;
import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by julio on 13/10/15.
 */
public abstract class BaseFragment extends Fragment {

    public abstract void onBackPressedCallback();

    protected void showExitApplicationDialog() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setTitle(getActivity().getString(R.string.warning));
        alertDialogBuilder
                .setMessage(getActivity().getString(R.string.want_to_quit_the_application))
                .setCancelable(false)
                .setPositiveButton(getActivity().getString(R.string.yes), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        getActivity().finish();
                    }
                })
                .setNegativeButton(getActivity().getString(R.string.no), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    public void showExpiredMessage() {
        final Dialog dialog = new Dialog(getActivity());
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_custom_expired_app, null);
        dialog.setContentView(view);
        dialog.setTitle(getString(R.string.pa_mobile_info));

        ViewHolder viewHolder = new ViewHolder(view);
        viewHolder.okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("market://details?id=br.com.principiosativosmobileCompleto"));
                startActivity(intent);
            }
        });
        dialog.show();
    }

    class ViewHolder {
        
        @InjectView(R.id.ok_button)
        public Button okButton;

        public ViewHolder(View view) {
            ButterKnife.inject(this, view);
        }
    }
}