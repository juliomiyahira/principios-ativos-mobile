package br.com.principiosativosmobile.fragments;

import android.graphics.PorterDuff;
import android.media.Image;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import br.com.principiosativosmobile.R;
import br.com.principiosativosmobile.adapters.MenuItemListAdapter;
import br.com.principiosativosmobile.models.Menu;
import br.com.principiosativosmobile.models.MenuItem;
import br.com.principiosativosmobile.utils.Constants;
import br.com.principiosativosmobile.utils.ImageUtil;
import br.com.principiosativosmobile.utils.PaFragmentManager;

/**
 * Created by julio on 09/10/15.
 */
public class ActivesCometicsItemDescriptionFragment extends BaseFragment{

    private MenuItem menuItem;

    private Menu menu;

    private ImageView menuImage;

    private TextView menuText;

    private TextView nameValue;

    private TextView descriptionlValue;

    private TextView concentrationValue;

    private TextView phValue;

    private Button backButton;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_activies_item_description_fragment, container, false);
        rootView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        setupView(rootView, container);
        return rootView;
    }

    private void setupView(View rootView, ViewGroup container) {
        menuImage = (ImageView) rootView.findViewById(R.id.menu_image);
        menuText = (TextView) rootView.findViewById(R.id.menu_text);
        nameValue = (TextView) rootView.findViewById(R.id.name_value);
        descriptionlValue = (TextView) rootView.findViewById(R.id.descriptionl_value);
        concentrationValue = (TextView) rootView.findViewById(R.id.concentration_value);
        phValue = (TextView) rootView.findViewById(R.id.ph_value);

        long menuItemId = getArguments().getLong(Constants.Extra.MENU_ITEM_ID, -1l);
        if(menuItemId != -1) {
            menuItem = MenuItem.findById(menuItemId);
            menu = Menu.buscarPorId(menuItem.menuId);
        }

        if(menuItem != null) {
            menuImage.setImageResource(menu.imageId);
            menuImage.getDrawable().setColorFilter(getResources().getColor(R.color.image_color_filter), PorterDuff.Mode.MULTIPLY);

            menuText.setText(menu.nome);
            nameValue.setText(menuItem.nome);
            descriptionlValue.setText(menuItem.descricao);
            concentrationValue.setText(menuItem.concentracao);
            phValue.setText(menuItem.ph);
        }
    }

    @Override
    public void onBackPressedCallback() {
        Bundle bundle = new Bundle();
        bundle.putLong(Constants.Extra.MENU_ID, menu.id);
        PaFragmentManager.getInstance().setCurrentFragment(new ActivesCometicsItemFragment());
        PaFragmentManager.getInstance().getCurrentFragment().setArguments(bundle);
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.container, PaFragmentManager.getInstance().getCurrentFragment());
        fragmentTransaction.commit();
    }
}
