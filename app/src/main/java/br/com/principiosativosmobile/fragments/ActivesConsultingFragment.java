package br.com.principiosativosmobile.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import br.com.principiosativosmobile.R;
import br.com.principiosativosmobile.utils.GmailSender;
import br.com.principiosativosmobile.utils.PaFragmentManager;

/**
 * Created by julio on 20/10/15.
 */
public class ActivesConsultingFragment extends BaseFragment implements View.OnClickListener {

    private ProgressDialog progressDialog;

    private EditText nameValue;

    private EditText emailValue;

    private EditText messageValue;

    private Button backButton;

    private Context context;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getActivity().setTitle(getString(R.string.consulting_title));
        View rootView = inflater.inflate(R.layout.fragment_consulting, container, false);
        rootView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        context = container.getContext();
        setupView(rootView);
        return rootView;
    }

    private void setupView(View view) {
        nameValue = (EditText) view.findViewById(R.id.name_value);
        emailValue = (EditText) view.findViewById(R.id.email_value);
        messageValue = (EditText) view.findViewById(R.id.message_value);
        backButton = (Button) view.findViewById(R.id.back_button);
        backButton.setOnClickListener(this);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onBackPressedCallback() {
        PaFragmentManager.getInstance().setCurrentFragment(new ActivesCometicsFragment());
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.container, PaFragmentManager.getInstance().getCurrentFragment());
        fragmentTransaction.commit();
    }

    @Override
    public void onClick(View v) {

        if (nameValue.getText() != null && !nameValue.getText().toString().isEmpty() && emailValue.getText() != null && !emailValue.getText().toString().isEmpty() && messageValue.getText() != null && !messageValue.getText().toString().isEmpty()) {
            String[] parans = new String[]{nameValue.getText().toString(), emailValue.getText().toString(), messageValue.getText().toString()};
            SendMailSync sendMailSync = new SendMailSync();
            sendMailSync.execute(parans);
            progressDialog = ProgressDialog.show(getActivity(), getString(R.string.warning), getString(R.string.send_email));
        } else {
            Toast.makeText(context, getString(R.string.consulting_message_alert_send_email), Toast.LENGTH_SHORT).show();
        }
    }

    class SendMailSync extends AsyncTask<String, Void, Boolean> {

        private Handler handler;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            handler = new Handler();
        }

        @Override
        protected Boolean doInBackground(String... params) {
            try {
                GmailSender gmailSender = new GmailSender();
                gmailSender.sendMail(getString(R.string.email_title_request_consulting), getString(R.string.email_content_consulting, params[0], params[1], params[2]), params[1], GmailSender.user);
                return true;
            } catch (Exception e) {
            }
            return false;
        }

        @Override
        protected void onPostExecute(final Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            handler.post(new Runnable() {
                @Override
                public void run() {
                    progressDialog.dismiss();
                    if (aBoolean) {
                        nameValue.setText(null);
                        emailValue.setText(null);
                        messageValue.setText(null);
                        Toast.makeText(context, getString(R.string.email_send_successfull), Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(context, getString(R.string.email_send_erro), Toast.LENGTH_LONG).show();
                    }
                }
            });
        }
    }
}
