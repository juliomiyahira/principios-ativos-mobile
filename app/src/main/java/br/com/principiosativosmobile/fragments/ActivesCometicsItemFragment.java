package br.com.principiosativosmobile.fragments;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.List;

import br.com.principiosativosmobile.PrincipiosAtivosMobileApplication;
import br.com.principiosativosmobile.R;
import br.com.principiosativosmobile.adapters.MenuItemListAdapter;
import br.com.principiosativosmobile.models.Menu;
import br.com.principiosativosmobile.models.MenuItem;
import br.com.principiosativosmobile.utils.Constants;
import br.com.principiosativosmobile.utils.Dataload;
import br.com.principiosativosmobile.utils.PaFragmentManager;

/**
 * Created by kenji on 04/10/15.
 */
public class ActivesCometicsItemFragment extends BaseFragment implements AdapterView.OnItemClickListener {

    public Menu menu;

    public List<MenuItem> menuItens;

    public ImageView imageView;

    public TextView nome;

    public ListView menuItemList;

    private MenuItemListAdapter adapter;

    private boolean mSearchCheck;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_activies_item_fragment, container, false);
        rootView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        nome = (TextView) rootView.findViewById(R.id.nome);
        imageView = (ImageView) rootView.findViewById(R.id.menu_image);
        menuItemList = (ListView) rootView.findViewById(R.id.menu_item_list);
        setupView(container);
        return rootView;
    }

    private void setupView(ViewGroup container) {
        long menuId = getArguments().getLong(Constants.Extra.MENU_ID, -1l);
        if(menuId != -1l){
            menu = Menu.buscarPorId(menuId);
        }
        menuItens = MenuItem.listarPorIdMenu(menu.id);
        nome.setText(menu.nome);
        imageView.setImageResource(menu.imageId);
        imageView.getDrawable().setColorFilter(null);

        adapter = new MenuItemListAdapter(container.getContext(), R.layout.menu_item_list, menuItens);
        menuItemList.setAdapter(adapter);
        menuItemList.setOnItemClickListener(this);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(android.view.Menu menu, MenuInflater inflater) {
        // TODO Auto-generated method stub
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu, menu);

        //Select search item
        final android.view.MenuItem menuItem = menu.findItem(R.id.menu_search);
        menuItem.setVisible(true);

        SearchView searchView = (SearchView) menuItem.getActionView();
        searchView.setQueryHint(this.getString(R.string.search));

        ((EditText) searchView.findViewById(R.id.search_src_text)).setHintTextColor(getResources().getColor(R.color.nliveo_white));
        searchView.setOnQueryTextListener(onQuerySearchView);
        mSearchCheck = false;
    }

    @Override
    public boolean onOptionsItemSelected(android.view.MenuItem item) {
        // TODO Auto-generated method stub

        switch (item.getItemId()) {
            case R.id.menu_search:
                mSearchCheck = true;
                break;
        }
        return true;
    }

    private SearchView.OnQueryTextListener onQuerySearchView = new SearchView.OnQueryTextListener() {
        @Override
        public boolean onQueryTextSubmit(String s) {
            return false;
        }

        @Override
        public boolean onQueryTextChange(String s) {
            if ((mSearchCheck) && (s != null) && (!s.isEmpty())) {
                menuItens = MenuItem.findByName(s, menu.id);
            } else {
                menuItens = MenuItem.listarPorIdMenu(menu.id);
            }
            adapter.clear();
            adapter.addAll(menuItens);
            adapter.notifyDataSetChanged();
            return false;
        }
    };

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
        if (PrincipiosAtivosMobileApplication.isFreeApp()) {
            if (Dataload.queryCountHasExpired()) {
                super.showExpiredMessage();
            } else {
                Dataload.incrementQueryCount();
                Bundle bundle = new Bundle();
                bundle.putLong(Constants.Extra.MENU_ITEM_ID, menuItens.get(position).id);

                PaFragmentManager.getInstance().setCurrentFragment(new ActivesCometicsItemDescriptionFragment());
                PaFragmentManager.getInstance().getCurrentFragment().setArguments(bundle);

                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.container, PaFragmentManager.getInstance().getCurrentFragment());
                fragmentTransaction.commit();
            }
        } else {
            Bundle bundle = new Bundle();
            bundle.putLong(Constants.Extra.MENU_ITEM_ID, menuItens.get(position).id);

            PaFragmentManager.getInstance().setCurrentFragment(new ActivesCometicsItemDescriptionFragment());
            PaFragmentManager.getInstance().getCurrentFragment().setArguments(bundle);

            FragmentManager fragmentManager = getFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.container, PaFragmentManager.getInstance().getCurrentFragment());
            fragmentTransaction.commit();
        }
    }

    @Override
    public void onBackPressedCallback() {
        PaFragmentManager.getInstance().setCurrentFragment(new ActivesCometicsFragment());
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.container, PaFragmentManager.getInstance().getCurrentFragment());
        fragmentTransaction.commit();
    }
}
