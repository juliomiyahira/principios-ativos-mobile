package br.com.principiosativosmobile.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import br.com.principiosativosmobile.R;
import br.com.principiosativosmobile.utils.PaFragmentManager;

/**
 * Created by diegohfranco on 09/10/15.
 */
public class ActiveBibliografiaFragment extends BaseFragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getActivity().setTitle(getString(R.string.bibliografics_title));
        View rootView = inflater.inflate(R.layout.fragment_bibliografia, container, false);
        rootView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onBackPressedCallback() {
        PaFragmentManager.getInstance().setCurrentFragment(new ActivesCometicsFragment());
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.container, PaFragmentManager.getInstance().getCurrentFragment());
        fragmentTransaction.commit();
    }
}
