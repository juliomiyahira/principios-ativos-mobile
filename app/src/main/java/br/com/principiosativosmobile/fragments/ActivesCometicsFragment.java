package br.com.principiosativosmobile.fragments;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.List;

import br.com.principiosativosmobile.R;
import br.com.principiosativosmobile.adapters.MenuListAdapter;
import br.com.principiosativosmobile.utils.Constants;
import br.com.principiosativosmobile.utils.PaFragmentManager;

/**
 * Created by kenji on 04/10/15.
 */
public class ActivesCometicsFragment extends BaseFragment implements AdapterView.OnItemClickListener {

    public ListView menuList;

    private List<br.com.principiosativosmobile.models.Menu> menus;

    private boolean mSearchCheck;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getActivity().setTitle(getString(R.string.actives_cometics_title));
        View rootView = inflater.inflate(R.layout.fragment_activies_fragment, container, false);
        rootView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        menuList = (ListView) rootView.findViewById(R.id.menu_list);
        setupView(container);
        return rootView;
    }

    private void setupView(ViewGroup container) {
        menus = br.com.principiosativosmobile.models.Menu.listarTodos();
        MenuListAdapter adapter = new MenuListAdapter(container.getContext(), R.layout.menu_list, menus);
        menuList.setAdapter(adapter);
        menuList.setOnItemClickListener(this);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        Bundle bundle = new Bundle();
        bundle.putLong(Constants.Extra.MENU_ID, menus.get(i).id);

        PaFragmentManager.getInstance().setCurrentFragment(new ActivesCometicsItemFragment());
        PaFragmentManager.getInstance().getCurrentFragment().setArguments(bundle);
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.container, PaFragmentManager.getInstance().getCurrentFragment());
        fragmentTransaction.commit();
    }

    @Override
    public void onBackPressedCallback() {
        showExitApplicationDialog();
    }
}
