package br.com.principiosativosmobile.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import br.com.principiosativosmobile.R;
import br.com.principiosativosmobile.models.MenuItem;
import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by julio on 25/09/15.
 */
public class MenuItemListAdapter extends ArrayAdapter<MenuItem> {

    public MenuItemListAdapter(Context context, int resource, List<MenuItem> objects) {
        super(context, resource, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = LayoutInflater.from(getContext()).inflate(R.layout.menu_item_list, null);
        ViewHolder viewHolder = new ViewHolder(convertView);
        viewHolder.nome.setText(getItem(position).nome);
        return convertView;
    }

    class ViewHolder {

        @InjectView(R.id.nome)
        public TextView nome;

        public ViewHolder(View view) {
            ButterKnife.inject(this, view);
        }
    }
}

