package br.com.principiosativosmobile.adapters;

import android.content.Context;
import android.graphics.PorterDuff;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

import br.com.principiosativosmobile.R;
import br.com.principiosativosmobile.models.Menu;
import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by julio on 25/09/15.
 */
public class MenuListAdapter extends ArrayAdapter<Menu> {

    public MenuListAdapter(Context context, int resource, List<Menu> objects) {
        super(context, resource, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = LayoutInflater.from(getContext()).inflate(R.layout.menu_list, null);
        ViewHolder viewHolder = new ViewHolder(convertView);
        viewHolder.nome.setText(getItem(position).nome);
        viewHolder.image.setImageResource(getItem(position).imageId);
        viewHolder.image.getDrawable().setColorFilter(null);
        return convertView;
    }

    class ViewHolder {

        @InjectView(R.id.image)
        public ImageView image;

        @InjectView(R.id.nome)
        public TextView nome;

        public ViewHolder(View view) {
            ButterKnife.inject(this, view);
        }
    }
}
