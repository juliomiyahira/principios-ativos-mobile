package br.com.principiosativosmobile.ui_utils;

/**
 * Created by kenji on 01/10/15.
 */
public interface OnItemClickListener {
    /**
     * Item click Navigation (ListView.OnItemClickListener)
     * @param position position of the item that was clicked.
     */
    public void onItemClick(int position);
}
